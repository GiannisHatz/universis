import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudyProgramsPreviewGeneralSpecialtiesComponent } from './study-programs-preview-general-specialties.component';

describe('StudyProgramsPreviewGeneralSpecialtiesComponent', () => {
  let component: StudyProgramsPreviewGeneralSpecialtiesComponent;
  let fixture: ComponentFixture<StudyProgramsPreviewGeneralSpecialtiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudyProgramsPreviewGeneralSpecialtiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudyProgramsPreviewGeneralSpecialtiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
