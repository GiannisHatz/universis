import {Component, OnDestroy, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-study-programs-preview-general-specialties',
  templateUrl: './study-programs-preview-general-specialties.component.html',
  styleUrls: ['./study-programs-preview-general-specialties.component.scss']
})
export class StudyProgramsPreviewGeneralSpecialtiesComponent implements OnInit, OnDestroy {

  public specialties: any;
  public studyProgramID: any;
  private subscription: Subscription;

  constructor( private _activatedRoute: ActivatedRoute,
               private _context: AngularDataContext) { }

  async ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.studyProgramID = params.id;
      this.specialties = await this._context.model('StudyProgramSpecialties')
        .where('studyProgram').equal(this.studyProgramID)
        .prepare()
        .take(-1)
        .getItems();
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

}
