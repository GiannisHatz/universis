import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScholarshipsPreviewStudentsComponent } from './scholarships-preview-students.component';

describe('ScholarshipsPreviewStudentsComponent', () => {
  let component: ScholarshipsPreviewStudentsComponent;
  let fixture: ComponentFixture<ScholarshipsPreviewStudentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScholarshipsPreviewStudentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScholarshipsPreviewStudentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
