import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScholarshipsPreviewGeneralComponent } from './scholarships-preview-general.component';

describe('ScholarshipsPreviewGeneralComponent', () => {
  let component: ScholarshipsPreviewGeneralComponent;
  let fixture: ComponentFixture<ScholarshipsPreviewGeneralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScholarshipsPreviewGeneralComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScholarshipsPreviewGeneralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
