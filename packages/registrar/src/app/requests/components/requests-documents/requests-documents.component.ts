import {Component, OnInit} from '@angular/core';
import * as REQUESTS_DOCUMENTS_LIST_CONFIG from '../../../requests/components/requests-documents/requests-documents.config.json';

@Component({
  selector: 'app-requests-documents',
  templateUrl: './requests-documents.component.html',
  styles: []
})
export class RequestsDocumentsComponent implements OnInit {
  public readonly config = REQUESTS_DOCUMENTS_LIST_CONFIG;

  constructor() { }

  ngOnInit() {
  }

}
