import {Component, OnInit} from '@angular/core';
import {AppEventService, DIALOG_BUTTONS, ModalService, TemplatePipe, ToastService} from '@universis/common';
import {ActivatedRoute, Router} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import {cloneDeep, template} from 'lodash';
import * as REGISTRATIONS_LIST_CONFIG from '../registrations-table/registrations-table.config.list.json';
import {TableConfiguration} from '@universis/ngx-tables';
import {ErrorService, LoadingService, UserActivityService} from '@universis/common';
import {TranslateService} from '@ngx-translate/core';
import {RegistrationsService} from '../../services/registrations.service';

@Component({
  selector: 'app-registrations-root',
  templateUrl: './registrations-root.component.html',
})
export class RegistrationsRootComponent implements OnInit {
  public registration: any;
  public actions: any[];
  public config: any;
  public allowedActions: any[];
  public edit: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _userActivityService: UserActivityService,
              private _translateService: TranslateService,
              private _registrationsService: RegistrationsService,
              private _errorService: ErrorService,
              private _loadingService: LoadingService,
              private _template: TemplatePipe,
              private _appEvent: AppEventService,
              private _modalService: ModalService,
              private _toastService: ToastService,
              private _router: Router) {
  }

  async ngOnInit() {
    // @ts-ignore
    this.config = cloneDeep(REGISTRATIONS_LIST_CONFIG as TableConfiguration);

    this._activatedRoute.params.subscribe((params) => {
      this._context.model('StudentPeriodRegistrations')
        // .asQueryable()
        .where('id').equal(params.id)
        .expand('registrationYear,registrationPeriod, classes, documents, student($expand=studentStatus,person)')
        .getItem().then((registration) => {
        this.registration = registration;
        this._appEvent.change.next({
          model: 'RegistrationsPreviewChange',
          target: this.registration
        });

        if (this.config.columns && this.registration) {
          this.actions = this.config.columns.filter(x => {
            return x.actions;
          })
            // map actions
            .map(x => x.actions)
            // get list items
            .reduce((a, b) => b, 0);

          this.allowedActions = this.actions.filter(x => {
            if (x.role) {
              if (x.role === 'action' || x.role === 'method') {
                if (x.access && x.access.length > 0) {
                  let access = x.access;
                  access = access.filter(y => {
                    if (y.registration) {
                      if ((y.registration.find(z => z.alternateName === this.registration.status.alternateName))
                          // tslint:disable-next-line:triple-equals - ([] == '')
                          || (y.registration.find(z => z.classes == this.registration.classes)
                          // tslint:disable-next-line:triple-equals - ([] == '')
                          && y.registration.find(z => z.documents == this.registration.documents))) {
                        x.disabled = false;
                      } else {
                        x.disabled = true;
                      }
                    }
                    if (y.student && y.student.studentStatus && x.disabled === false) {
                      const allowedStatuses = y.student.studentStatus;
                      const studentStatus = this.registration.student &&
                        this.registration.student.studentStatus &&
                        this.registration.student.studentStatus.alternateName;
                      if (Array.isArray(allowedStatuses)) {
                        x.disabled = !allowedStatuses.includes(studentStatus);
                      } else {
                        x.disabled = allowedStatuses !== studentStatus;
                      }
                    }
                    return y;
                  });
                  if (access && access.length > 0) {
                    return x;
                  }
                } else {
                  return x;
                }
              }
            }
          });

          this.edit = this.actions.find(x => {
            if (x.role === 'edit') {
              if (x.href.includes('${student}')) {
                x.href = x.href.replace('${student}', '${student.id}');
              }
              x.href = template(x.href)(this.registration);
              return x;
            }
          });

          this.actions = this.allowedActions;
          this.actions.forEach(action => {
            if (action.role === 'action') {
              if (action.href.includes('${student}')) {
                action.href = action.href.replace('${student}', '${student.id}');
              }
              action.href = this._template.transform(action.href, this.registration);
            } else {
              action.click = () => {
                if (typeof this[action.invoke] === 'function') { this[action.invoke](); }
               //  const invokeMethod = this[action.invoke]; if (typeof invokeMethod === 'function') { invokeMethod.bind(this)(); }
              };
            }
          });

          if (this.registration) {
            return this._userActivityService.setItem({
              category: this._translateService.instant('Registrations.TitleOne'),
              // tslint:disable-next-line:max-line-length
              description: this._translateService.instant(this.registration.student.person.familyName + ' ' + this.registration.student.person.givenName),
              url: window.location.hash.substring(1),
              dateCreated: new Date()
            });
          }
        }
      }).catch((err) => {
        this._errorService.navigateToError(err);
      });
    });
  }

  disableRegistration() {
    this._loadingService.showLoading();
    const status = {'alternateName': 'closed'};
    this._registrationsService.changeRegistrationStatus(this.registration, status).then((resAction) => {
      this.registration.status.alternateName = status.alternateName;
      this._appEvent.change.next({
        model: 'RegistrationsPreviewChange',
        target: this.registration
      });
      this.ngOnInit();
      this._loadingService.hideLoading();
    }, (err) => {
      this._loadingService.hideLoading();
      return this._errorService.navigateToError(err);
    });
  }

  enableRegistration() {
    this._loadingService.showLoading();
    const status = {'alternateName': 'open'};
    this._registrationsService.changeRegistrationStatus(this.registration, status).then((resAction) => {
      this.registration.status.alternateName = status.alternateName;
      this._appEvent.change.next({
        model: 'RegistrationsPreviewChange',
        target: this.registration
      });
      this.ngOnInit();
      this._loadingService.hideLoading();
    }, (err) => {
      this._loadingService.hideLoading();
      return this._errorService.navigateToError(err);
    });
  }

  deleteRegistration() {
    // always show warning dialog before deletion
    return this._modalService.showWarningDialog(
      this._translateService.instant('Registrations.DeleteAction.ModalTitle'),
      this._translateService.instant('Registrations.DeleteAction.ModalDescription'),
      DIALOG_BUTTONS.OkCancel).then( result => {
      // if the user confirms the action, proceed
      if (result === 'ok') {
        // show loading
        this._loadingService.showLoading();
        // prepare minimal object
        const deleteRegistration = {
          id: this.registration.id,
          $state: 4
        };
        // and save
        return this._context.model('StudentPeriodRegistrations')
          .save(deleteRegistration).then(() => {
            // hide loading
            this._loadingService.hideLoading();
            // show toast message
            this._toastService.show(this._translateService.instant('Registrations.DeleteAction.ModalTitle'),
              this._translateService.instant('Registrations.DeleteAction.ModalSuccess'), false, 5000);
            // do not reload data, or fire an app event. The data is deleted, let setTimeout navigate
            setTimeout(() => {
              // navigate to registrations list after 800ms
              // a bit smoother experience
              this._router.navigate(['registrations']);
            }, 800);
          }).catch(err => {
            this._loadingService.hideLoading();
            this._errorService.showError(err, {
              continueLink: '.'
            });
          });
      }
    });
  }
}
