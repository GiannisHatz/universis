import {Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {
  AddItemsComponent, AdvancedRowActionComponent,
  AdvancedTableComponent,
  AdvancedTableConfiguration,
  AdvancedTableDataResult
} from '@universis/ngx-tables';
import {ActivatedRoute} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {AngularDataContext} from '@themost/angular';
import * as GRADUATIONS_REQUEST_LIST_CONFIG from './graduations-requests.config.json';
import * as GRADUATIONS_REQUEST_LIST_SEARCH from './graduations-requests.search.json';
import {AdvancedSearchFormComponent} from '@universis/ngx-tables';
import {ClientDataQueryable} from '@themost/client';
import {Observable, Subscription} from 'rxjs';
import {RequestActionComponent} from '../../../../requests/components/request-action/request-action.component';
import {AdvancedTableSearchComponent} from '@universis/ngx-tables';
import {
  ConfigurationService,
  ErrorService,
  LoadingService,
  ModalService,
  ToastService,
  UserActivityService,
  UserService
} from '@universis/common';
import {ActivatedTableService} from '@universis/ngx-tables';
import {MessagesService} from '../../../../messages/services/messages.service';
import {SendMessageToStudentComponent} from '../../../../messages/components/send-message-to-student/send-message-to-student.component';
import {ActiveDepartmentService} from '../../../../registrar-shared/services/activeDepartmentService.service';
import {DatePipe} from '@angular/common';
import {StudentsSharedModule} from '../../../../students/students.shared';

@Component({
  selector: 'app-graduations-student-requests',
  templateUrl: './graduations-student-requests.component.html',
  styles: [`
    .scrollHorizontal {
      max-height: 340px;
      overflow-y: scroll;
    }
    `]
})
export class GraduationsStudentRequestsComponent implements OnInit, OnDestroy {

  private dataSubscription: Subscription;
  private fragmentSubscription: Subscription;
  @ViewChild('table') table: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  public graduationEventId: any;
  public graduationEvent: any;
  @ViewChild('advancedSearch') advancedSearch: AdvancedTableSearchComponent;
  @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();
  private subscription: Subscription;

  public allowActive = true;
  public recordsTotal: any;
  private selectedItems = [];
  private dataTables = [];
  public canCreateRequest = false;
  public alertMessage = {
    message : '',
    ccsClass : ''
  };

  constructor(private _context: AngularDataContext,
              private _activatedRoute: ActivatedRoute,
              private _modalService: ModalService,
              private _activatedTable: ActivatedTableService,
              private _loadingService: LoadingService,
              private _translateService: TranslateService,
              private _userActivityService: UserActivityService,
              private _userService: UserService,
              private _errorService: ErrorService,
              private _toastService: ToastService,
              private _messagesService: MessagesService,
              private _activeDepartment: ActiveDepartmentService,
              private _configurationService: ConfigurationService,
              public datepipe: DatePipe) {
  }

  async ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.graduationEventId = params.id;
      this.table.query = this._context.model('GraduationRequestActions')
        .where('graduationEvent').equal(this.graduationEventId)
        .expand('student($expand=studyProgram,department,person,semester,requeststatus,inscriptionYear,inscriptionPeriod,studentStatus)')
        .prepare();

      this.table.config = AdvancedTableConfiguration.cast(GRADUATIONS_REQUEST_LIST_CONFIG);
      this.search.form = AdvancedTableConfiguration.cast(GRADUATIONS_REQUEST_LIST_SEARCH);

      this.graduationEvent = await this._context.model('GraduationEvents')
        .select('attachmentTypes, studyPrograms')
        .where('id').equal(this.graduationEventId)
        .getItem();

      if (this.graduationEvent) {
        if (this.graduationEvent.eventStatus.alternateName !== 'EventOpened') {
          this.alertMessage.message = this._translateService.instant('Graduations.Alerts.CannotCreateRequest');
          this.alertMessage.ccsClass = 'alert-danger';
          this.canCreateRequest = false;
        } else {
          this.canCreateRequest = true;
          if (this.graduationEvent.validFrom && this.graduationEvent.validThrough) {
            const currentDate = new Date();   // get only date format
            currentDate.setHours(0, 0, 0, 0);
            const validFrom = new Date(this.graduationEvent.validFrom);
            validFrom.setHours(0, 0, 0, 0);
            const validThrough = new Date(this.graduationEvent.validThrough);
            validThrough.setHours(0, 0, 0, 0);

            // check dates is invalid
            if (currentDate < validFrom || currentDate > validThrough) {
              this.alertMessage.message = this._translateService.instant('Graduations.Alerts.WrongDates');
              this.alertMessage.ccsClass = 'alert-warning';
            }
          }

          // check attachments is invalid
          if (this.graduationEvent.attachmentTypes && this.graduationEvent.attachmentTypes.length > 0) {
            this.alertMessage.message = this._translateService.instant('Graduations.Alerts.CannotCreateRequestWithAttachments');
            this.alertMessage.ccsClass = 'alert-danger';
            this.canCreateRequest = false;
          }

          // data is valid and can create new request
          if (this.alertMessage.message.length === 0) {
            this.alertMessage.message = this._translateService.instant('Graduations.Alerts.CanCreateRequest');
            this.alertMessage.ccsClass = 'alert-success';
          }
        }
      }

      this.dataSubscription = this._activatedRoute.data.subscribe(data => {
        this._activatedTable.activeTable = this.table;
        // set search form
        if (data.searchConfiguration) {
          this.search.form = data.searchConfiguration;
          this.search.ngOnInit();
        }
        // set table config and recall data
        if (data.tableConfiguration) {
          this.table.config = data.tableConfiguration;
          this.advancedSearch.getQuery().then(res => {
            this.table.destroy();
            this.table.query = res;
            this.advancedSearch.text = '';
            this.table.fetch(false);
          });
        }

        this.fragmentSubscription = this._activatedRoute.fragment.subscribe(fragment => {
          if (fragment && fragment === 'reload') {
            this.table.fetch(true);
          }
        });
      });
    });
    const services = await this._context.model('diagnostics/services').getItems();
    if (services && services.length) {
      this.allowActive = !services.find(x => {
        return x.serviceType === 'ValidateGraduationRequestStudentStatus';
      });
    }
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
    this.dataTables = data.data;
  }

  async getSelectedItems() {
    let items = [];
    if (this.table && this.table.lastQuery) {
      const lastQuery: ClientDataQueryable = this.table.lastQuery;
      // search for document attributes (if table has published column)

      if (lastQuery != null) {
        if (this.table.smartSelect) {
          // get items
          const selectArguments = ['id', 'actionStatus/alternateName as actionStatus',
            'student/studentStatus/alternateName as studentStatus', 'student/id as studentId', 'createdBy/id as createdBy'];

          // query items
          const queryItems = await lastQuery.select.apply(lastQuery, selectArguments)
            .take(-1)
            .skip(0)
            .getItems();
          if (this.table.unselected && this.table.unselected.length) {
            // remove items that have been unselected by the user
            items = queryItems.filter(item => {
              return this.table.unselected.findIndex((x) => {
                return x.id === item.id;
              }) < 0;
            });
          } else {
            items = queryItems;
          }
        } else {
          // get selected items only
          items = this.table.selected.map((item) => {
            return {
              id: item.id,
              actionStatus: item.actionStatus,
              studentStatus: item.studentStatus,
              student: item.studentId,
              createdBy: item.createdBy
            };
          });
        }
      }
    }
    return items;
  }

  executeChangeActionStatus(statusText: string) {
    return new Observable((observer) => {
      this.refreshAction.emit({
        progress: 1
      });
      const total = this.selectedItems.length;
      const result = {
        total: total,
        success: 0,
        errors: 0
      };

      // execute promises in series within an async method
      (async () => {
        const user = await this._userService.getUser();

        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            // set active item
            const updated = {
              id: item.id,
              actionStatus: {
                alternateName: statusText
              },
              agent: {
                id: user.id,
                name: user.name
              }
            };
            await this._context.model(this.table.config.model).save(updated);
            result.success += 1;
            // do not throw error while updating row
            // (user may refresh view)
            try {
              await this.table.fetchOne({
                id: updated.id
              });
            } catch (err) {
              //
            }

          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }

        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  /***
   * Accepts the selected requests
   */
  async acceptAction() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      // get only active items
      this.selectedItems = items.filter((item) => {
        return item.actionStatus === 'ActiveActionStatus' && ((!this.allowActive && item.studentStatus === 'declared') ||
          (this.allowActive && (item.studentStatus === 'active' || item.studentStatus === 'declared')));
      });
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(RequestActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Graduations.Edit.AcceptAction.Title',
          description: 'Graduations.Edit.AcceptAction.Description',
          refresh: this.refreshAction,
          execute: this.executeChangeActionStatus('CompletedActionStatus')
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  /**
   * Rejects the selected requests
   */
  async rejectAction() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      // get only active items
      this.selectedItems = items.filter((item) => {
        return item.actionStatus === 'ActiveActionStatus';
      });
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(RequestActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Graduations.Edit.RejectAction.Title',
          description: 'Graduations.Edit.RejectAction.Description',
          refresh: this.refreshAction,
          execute: this.executeChangeActionStatus('CancelledActionStatus')
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  async validateRequest() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      // get only active items
      this.selectedItems = items.filter((item) => {
        return item.actionStatus === 'ActiveActionStatus';
      });
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(RequestActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Graduations.Edit.ValidateAction.Title',
          description: 'Graduations.Edit.ValidateAction.Description',
          refresh: this.refreshAction,
          execute: this.executeValidateRequest()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  executeValidateRequest() {
    return new Observable((observer) => {
      this.refreshAction.emit({
        progress: 1
      });
      const total = this.selectedItems.length;
      const result = {
        total: total,
        success: 0,
        errors: 0
      };

      // execute promises in series within an async method
      (async () => {
        const user = await this._userService.getUser();

        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            const requestValidations = await this._context.model(`GraduationRequestActions/${item.id}/validate`).getItems();
            result.success += 1;
            // do not throw error while updating row
            // (user may refresh view)
            try {
              await this.table.fetchOne({
                id: item.id
              });
            } catch (err) {
              //
            }

          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  async communicateWithAttendees() {
    try {
      this._loadingService.showLoading();
      this.selectedItems = await this.getSelectedItems();
      const selectedStudentIds = [];
      for (const item of this.selectedItems) {
        selectedStudentIds.push(item.student || item.studentId);
      }
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(SendMessageToStudentComponent, {
        class: 'modal-xl',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          studentId: selectedStudentIds,
          showMessageForm: true,
          showLoading: false,
          modalTitle: 'Graduations.Edit.CommunicateWithAttendees.Title',
          description: 'Graduations.Edit.CommunicateWithAttendees.Description'
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  async add() {
    try {
      const tableConfiguration = AdvancedTableConfiguration.cast(StudentsSharedModule.ActiveDeclaredStudentsList, true);
      // tslint:disable-next-line:max-line-length
      tableConfiguration.defaults.filter += ` and (semester ge $it/studyProgram/semesters or semester ge $it/studyProgram/info/minimumAllowedSemester)`;

      if (this.graduationEvent.studyPrograms) {
        let stFilter = '';
        this.graduationEvent.studyPrograms.forEach(st => {
          stFilter += stFilter.length > 0 ? ' or ' : '';
          stFilter += ` (studyProgram eq ${st.id}) `;
        });
        stFilter = stFilter.length > 0 ? ' and (' + stFilter + ')' : '';
        tableConfiguration.defaults.filter += stFilter;
      }

      this._modalService.openModalComponent(AddItemsComponent, {
        class: 'modal-xl modal-table',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          tableConfiguration: tableConfiguration,
          modalTitle: 'Graduations.AddStudents.Title',
          refresh: this.refreshAction,
          execute: (() => {
            return new Observable((observer) => {
              try {
                this._loadingService.showLoading();
                const component = <AddItemsComponent>this._modalService.modalRef.content;
                (async () => {

                  const user = await this._userService.getUser();
                  const success = [];
                  const unsuccess = [];
                  if (component.items && component.items.length > 0) {
                    for (const studentItem of component.items) {
                      try {
                        let studentExist;
                        if (this.dataTables) {
                          studentExist = this.dataTables.filter(d => {
                            if (d.studentId === studentItem.id) {
                              return d;
                            }
                          });
                        }

                        if (!studentExist || studentExist.length === 0) {
                          // get graduation rules for every student
                          const graduationRules = await this._context.model(`Students/${studentItem.id}/graduationRules`).getItems();

                          if (graduationRules && graduationRules.finalResult && graduationRules.finalResult.success) {
                            const student = {
                              graduationEvent: this.graduationEvent,
                              student: studentItem,
                              name: this._translateService.instant('Graduations.RequestTitle'),
                              description: this._translateService.instant('Graduations.RequestDescription'),
                              actionStatus: {
                                alternateName: 'ActiveActionStatus'
                              },
                              agent: {
                                id: user.id,
                                name: user.name
                              }
                            };
                            // save student graduation request
                            const result = await this._context.model('GraduationRequestActions').save(student);
                            if (result) {
                              success.push(result);
                            } else {
                              unsuccess.push(studentItem);
                            }
                          } else {
                            unsuccess.push(studentItem);
                          }
                        } else {
                          unsuccess.push(studentItem);
                        }

                      } catch (err) {
                        this._loadingService.hideLoading();
                        this._errorService.showError(err, {
                          continueLink: '.'
                        });
                      }
                    } // end for
                    let dialogDescription = '';
                    // found items that were passed
                    if (success.length > 0) {
                      dialogDescription = this._translateService.instant(
                        (success.length === 1 ? 'Graduations.AddStudents.one' : 'Graduations.AddStudents.many')
                        , {value: success.length});
                    }

                    // found items that were not passed
                    if (unsuccess.length > 0) {
                      dialogDescription += dialogDescription.length > 0 ? '<br>' : '';
                      dialogDescription += this._translateService.instant(
                        (unsuccess.length === 1 ? 'Graduations.AddStudents.oneFail' : 'Graduations.AddStudents.manyFail')
                        , {value: unsuccess.length});

                      dialogDescription += `<div class="pre-scrollable">`;
                      unsuccess.forEach(item => {
                        dialogDescription += item.studentIdentifier + ' - ' + item.familyName + ' ' + item.givenName + '<br>';
                      });
                      dialogDescription += '</div>';
                    }

                    this._loadingService.hideLoading();

                    // show results
                    this._modalService.showDialog(
                      this._translateService.instant('Graduations.AddStudents.Title'),
                      dialogDescription
                    );

                    this.table.fetch(true);
                  } // end if items length
                })().then(() => {
                  observer.next();
                }).catch((err) => {
                  observer.error(err);
                });
                observer.next();
              } catch (e) {
                observer.error(e);
              }
            });
          })()
        }
      });
    } catch (err) {
      if (this._modalService.modalRef) {
        this._modalService.modalRef.hide();
      }
      this._errorService.showError(err, {
        continueLink: '.'
      });
      this._loadingService.hideLoading();
    }
  }

  /**
   * Add student requests from GraduationRuleValidateAction results
   */
  async addFromValidationResults() {
    try {
      // get active department
      const activeDepartment = await this._activeDepartment.getActiveDepartment();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          data: {
            'department': activeDepartment.id
          },
          formTemplate: 'GraduationEvents/addRequestsFromResults',
          modalTitle: 'Graduations.AddRequestsFromResults.Title',
          description: 'Graduations.AddRequestsFromResults.Description',
          errorMessage: 'Graduations.AddRequestsFromResults.CompletedWithErrors',
          refresh: this.refreshAction,
          execute: this.executeAddRequestsFromResults()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }


  /**
   * Executes adding graduation requests
   */
  executeAddRequestsFromResults() {
    return new Observable((observer) => {
      // get values from modal component
      const items = [];
      const total = 0;
      const result = {
        total: items.length,
        success: 0,
        errors: 0
      };
      const component = this._modalService.modalRef.content as AdvancedRowActionComponent;
      const data = component.formComponent.form.formio.data;
      if (data.action == null || data.action === '') {
        result.errors = result.total;
        return observer.next(result);
      }
      this.refreshAction.emit({
        progress: 1
      });
      // execute promises in series within an async method
      (async () => {
        // get validation results
        const students = await this._context.model('GraduationRuleValidateResults').where('action').equal(data.action.id)
          .and('result').equal(1)
          .take(-1)
          .getItems();
        result.total = students.length;
        // get user
        const user = await this._userService.getUser();

        for (let index = 0; index < students.length; index++) {
          try {
            const item = students[index];
            // check if student request already exists
            const studentRequest = await this._context.model('GraduationRequestActions')
              .where('student').equal(item.student).and('graduationEvent').equal(this.graduationEventId).getItem();
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / result.total) * 100)
            });
            if (!studentRequest) {
              // check again if student graduation rules are met
              const graduationRules = await this._context.model(`Students/${item.student}/graduationRules`).getItems();
              if (graduationRules && graduationRules.finalResult && graduationRules.finalResult.success) {
                const student = {
                  graduationEvent: this.graduationEvent,
                  student: item.student,
                  name: this._translateService.instant('Graduations.RequestTitle'),
                  description: this._translateService.instant('Graduations.RequestDescription'),
                  actionStatus: {
                    alternateName: 'ActiveActionStatus'
                  },
                  agent: {
                    id: user.id,
                    name: user.name
                  }
                };
                // save student graduation request
                const request = await this._context.model('GraduationRequestActions').save(student);
                if (request) {
                  result.success += 1;
                } else {
                  result.errors += 1;
                }
              } else {
                result.errors += 1;
              }
              result.success += 1;
            } else {
              result.errors += 1;
            }
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }
        }
        try {
          await this.table.fetch();
        } catch (err) {
          //
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }



  async removeRequests() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      // get only active items and items created by looged in user
      const user = await this._userService.getUser();
      this.selectedItems = items.filter((item) => {
        return item.actionStatus === 'ActiveActionStatus' && item.createdBy === user.id;
      });
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(RequestActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Graduations.RequestDeleteAction.Title',
          description: 'Graduations.RequestDeleteAction.Description',
          errorMessage: 'Graduations.RequestDeleteAction.CompletedWithErrors.Title',
          refresh: this.refreshAction,
          execute: this.executeRemoveRequest()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  executeRemoveRequest() {
    return new Observable((observer) => {
      this.refreshAction.emit({
        progress: 1
      });
      const total = this.selectedItems.length;
      const result = {
        total: total,
        success: 0,
        errors: 0
      };
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            await this._context.model('GraduationRequestActions').remove(item);
            result.success += 1;
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }
        }
        try {
          await this.table.fetch(true);
        } catch (err) {
          //
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

}
