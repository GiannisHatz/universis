import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-graduations-overview-capacity',
  templateUrl: './graduations-overview-capacity.component.html'
})
export class GraduationsOverviewCapacityComponent implements OnInit {

  @Input() graduationEvent: any;
  public remainingAttendeeCapacity = 0;
  public currentCapacity = 0;

  public chartEventCapacityOptions: any;
  public chartEventCapacityLabels = [] ;
  public chartEventCapacityData = [] ;
  public chartEventCapacityType = 'doughnut';
  public chartEventCapacityLegend = true;
  public chartEventCapacityColours: Array<any> = [
    {
      backgroundColor: ['#63c2de', '#f86c6b']
    }
  ];

  constructor() { }

  async ngOnInit() {

    this.chartEventCapacityOptions = {
      responsive: true,
      legend: {
        display: true,
        position: 'bottom'
      }
    };

    if (this.graduationEvent && this.graduationEvent.maximumAttendeeCapacity ) {
      if (this.graduationEvent && this.graduationEvent.remainingAttendeeCapacity) {
        this.remainingAttendeeCapacity = this.graduationEvent.remainingAttendeeCapacity;
        this.currentCapacity = this.graduationEvent.maximumAttendeeCapacity - this.graduationEvent.remainingAttendeeCapacity;
      } else {
        this.currentCapacity = this.graduationEvent.maximumAttendeeCapacity;
      }
    }
  }

}
