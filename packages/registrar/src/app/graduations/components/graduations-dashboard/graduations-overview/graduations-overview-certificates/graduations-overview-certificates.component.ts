import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-graduations-overview-certificates',
  templateUrl: './graduations-overview-certificates.component.html'
})
export class GraduationsOverviewCertificatesComponent implements OnInit {

  @Input() public graduationEvent: any;

  constructor() { }

  ngOnInit() {
  }

}
