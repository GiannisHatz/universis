import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AttachmentTypesOverviewComponent } from './attachment-types-overview.component';

describe('AttachmentTypesOverviewComponent', () => {
  let component: AttachmentTypesOverviewComponent;
  let fixture: ComponentFixture<AttachmentTypesOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttachmentTypesOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttachmentTypesOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
