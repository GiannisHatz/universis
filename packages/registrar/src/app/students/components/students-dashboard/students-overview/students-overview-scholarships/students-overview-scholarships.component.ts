import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {Subscription} from 'rxjs';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-students-overview-scholarships',
  templateUrl: './students-overview-scholarships.component.html',
  styleUrls: ['./students-overview-scholarships.component.scss']
})
export class StudentsOverviewScholarshipsComponent implements OnInit, OnDestroy  {

  public scholarships: any;
  @Input() studentId: number;
  private subscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) { }
  async ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.studentId = params.id;
      this.scholarships = await this._context.model('StudentScholarships')
        .asQueryable()
        .where('student').equal(this.studentId)
        .getItems();
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
