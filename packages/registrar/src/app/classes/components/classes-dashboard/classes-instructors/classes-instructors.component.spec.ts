import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassesInstructorsComponent } from './classes-instructors.component';

describe('ClassesInstructorsComponent', () => {
  let component: ClassesInstructorsComponent;
  let fixture: ComponentFixture<ClassesInstructorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClassesInstructorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassesInstructorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
