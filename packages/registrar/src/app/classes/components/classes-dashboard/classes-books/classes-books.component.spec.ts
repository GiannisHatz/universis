import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassesBooksComponent } from './classes-books.component';

describe('ClassesBooksComponent', () => {
  let component: ClassesBooksComponent;
  let fixture: ComponentFixture<ClassesBooksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClassesBooksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassesBooksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
