import { Component, Input, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { DatePipe } from '@angular/common';
import { AdvancedTableSearchBaseComponent } from '@universis/ngx-tables';

@Component({
    selector: 'app-classes-advanced-table-search',
    templateUrl: './classes-advanced-table-search.component.html',
    encapsulation: ViewEncapsulation.None
})

export class ClassesAdvancedTableSearchComponent extends AdvancedTableSearchBaseComponent {

    constructor(_context: AngularDataContext, _activatedRoute: ActivatedRoute, datePipe: DatePipe) {
        super();
    }
}
