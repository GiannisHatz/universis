import { Component, OnInit } from '@angular/core';
import 'smartwizard';

declare let $ : any;

@Component({
    selector: 'lib-wizard',
    templateUrl: './wizard.component.html',
    styleUrls: []
})
export class WizardComponent implements OnInit {
    
    constructor() {  }

    ngOnInit() {
        $('#smartwizard').smartWizard({
            showStepURLhash: false
        });
    }
}
