import {BrowserModule} from '@angular/platform-browser';
import {CUSTOM_ELEMENTS_SCHEMA, ModuleWithProviders, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {AppRoutingModule} from './app-routing.module';
import {AppSidebarModule} from '@coreui/angular';
import {AppComponent} from './app.component';
import {CheckboxesComponent} from './checkboxes/checkboxes.component';
import {FullLayoutComponent} from './layouts/full-layout.component';
import {HashLocationStrategy, LocationStrategy} from '@angular/common';
import {ColorsComponent} from './colors/colors.component';
import {DatepickerComponent} from './datepicker/datepicker.component';
import {WizardComponent} from './wizard/wizard.component';
import {CardsModule} from './cards/cards.module';
import {TypographyComponent} from './typography/typography.component';
import {BsDatepickerConfig, BsDatepickerModule, BsLocaleService, ModalModule, TabsModule} from 'ngx-bootstrap';
import {FormsUnifiedModule} from './forms/forms.module';
import {ButtonsComponent} from './buttons/buttons.component';
import {HighlightIncludeModule} from './highlight/highlight.module';
import {ListsModule} from './lists/lists.module';
import {FormsModule} from '@angular/forms';
import {AppSidebarService} from './app-sidebar-service/app.sidebar.service';
import {ModalsModule} from './modals/modals.module';
import {ChartsModule} from 'ng2-charts/ng2-charts';
import {PreviewsModule} from './previews/previews.module';


@NgModule({
  declarations: [
    AppComponent,
    ButtonsComponent,
    CheckboxesComponent,
    ColorsComponent,
    DatepickerComponent,
    FullLayoutComponent,
    TypographyComponent,
    WizardComponent
  ],
  imports: [
    ChartsModule,
    AppRoutingModule,
    AppSidebarModule,
    BrowserModule,
    BsDatepickerModule.forRoot(),
    CardsModule,
    FormsModule,
    FormsUnifiedModule,
    HttpClientModule,
    HighlightIncludeModule,
    ModalsModule,
    ListsModule,
    TabsModule.forRoot(),
    PreviewsModule,
    ModalModule.forRoot()
  ],
  providers: [
    AppSidebarService,
    BsLocaleService,
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy
    },
    BsDatepickerConfig
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
})
export class AppModule {
}
