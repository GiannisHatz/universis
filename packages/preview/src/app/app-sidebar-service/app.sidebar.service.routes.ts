// tslint:disable quotemark
export const APP_SIDEBAR_ITEMS = [
  {
    "name": "Buttons",
    "url": "/buttons",
    "index": 0
  },
  {
    "name": "Cards",
    "url": "javascript:void(0)",
    "index": 10,
    "children": [
      {
        "name": "Cards",
        "url": "/cards/cards",
        "index": 0
      },
      {
        "name": "Expandable Cards",
        "url": "/cards/expandable-cards",
        "index": 5
      },
      {
        "name": "Profile Cards",
        "url": "/cards/profile-cards",
        "index": 10
      }
    ]
  },
  {
    "name": "Checkboxes",
    "url": "/checkboxes",
    "index": 20
  },
  {
    "name": "Colors",
    "url": "/colors",
    "index": 30
  },
  {
    "name": "Datepicker",
    "url": "/datepicker",
    "index": 40
  },
  {
    "name": "FormIO",
    "url": "/formio",
    "index": 50
  },
  {
    "name": "Forms",
    "url": "javascript:void(0)",
    "index": 60,
    "children": [
      {
        "name": "Advanced Search",
        "url": "/forms/advanced-search",
        "index": 0
      },
      {
        "name": "Dropdown Form",
        "url": "/forms/dropdown-form",
        "index": 5
      },
      {
        "name": "Expandable Form",
        "url": "/forms/expandable-form",
        "index": 10
      },
      {
        "name": "Simple Form",
        "url": "/forms/simple-form",
        "index": 15
      }
    ]
  },
  {
    "name": "Lists",
    "url": "javascript:void(0)",
    "index": 70,
    "children": [
      {
        "name": "Group Lists",
        "url": "/lists/group-list",
        "index": 0
      },
      {
        "name": "Simple List",
        "url": "/lists/simple-list",
        "index": 5
      }
    ]
  },
  {
    "name": "Modals",
    "url": "javascript:void(0)",
    "index": 80,
    "children": [
      {
        "name": "Modal Dialog",
        "url": "/modals/modals-dialog",
        "index": 0
      },
      {
        "name": "Modal Doughnut",
        "url": "/modals/modals-doughnut",
        "index": 5
      }
    ]
  },
  {
    'name': 'Previews',
    'url': 'javascript:void(0)',
    'index': 60,
    'children': [
      {
        'name': 'Graduation Rules',
        'url': '/previews/graduation-rules',
        'index': 0
      },
      {
        'name': 'Graduation Progress',
        'url': '/previews/graduation-progress',
        'index': 5
      },
      {
        'name': 'Simple Graduation Rules',
        'url': '/previews/graduation-simplified',
        'index': 10
      },
    ]
  },
  {
    'name': 'Router Modals',
    'index': 100,
    'children': [
      {
        'name': 'Advanced',
        'url': '/router-modals/advanced',
        'index': 0
      },
      {
        'name': 'Simple',
        'url': '/router-modals/simple',
        'index': 5
      }
    ]
  },
  {
    "name": "Typography",
    "url": "/typography",
    "index": 110
  },
  {
    "name": "Wizard",
    "url": "/wizard",
    "index": 120
  }
];
// tslint:enable quotemark
